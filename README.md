Spring Starter
==============

### Introduction:
This application is a basic spring web mvc setup which can be easily copy pasted to quickly start spring web applications.


### Environmental Requirements:
For successful build, the system needs to have the latest version of following components installed
>+ JDK 8
+ Tomcat 8
+ Maven 3


### Preferred IDE:
Any IDE can be used. However, project was created using IntelliJ IDEA.


### Build & Deployment:
>+ Clone the project from [https://zobayer@bitbucket.org/zobayer/springstarter.git].
+ Once maven has finished resolving the dependencies, running the "install" task, or "mvn install" command on base directory will generate the *.war* file in {base_dir}/target/
+ Start your tomcat server, navigate to [http://localhost:8080/manager/html], authenticate, upload the war file and hit deploy.
+ To view the default page, navigate to [http://localhost:8080/springstarter].


### Creating New Project:
>+ {base_dir}/pom.xml -> You can change groupId, artifactId and version.
+ {base_dir}/src/main/webapp/WEB-INF/web.xml -> you can change <servlet-name> depending on your project.
+ {base_dir}/src/main/webapp/WEB-INF/<your-servlet-name>-servlet.xml -> you can change component-scan package name.
+ {base_dir}/src/main/java/ -> update the package name according to your project.


### Conclusion:
Once you successfully build and deploy the demo project, it should be easier to just convert it into another application.
If you face any difficulties, please send me an email [zobayer@tigeritbd.com].


### Tags:
[Spring] [Spring Web] [Spring Web-Mvc] [Maven] [Tomcat] [Java] [JDK] [Starter] [MVC]
